from decimal import Decimal
from typing import Dict, Any

import numpy as np
from braket.ahs import AnalogHamiltonianSimulation, AtomArrangement, DrivingField
from braket.circuits import Circuit
from braket.tasks import AnalogHamiltonianSimulationQuantumTaskResult
from braket.timings import TimeSeries
from qiskit import QuantumCircuit

from planqk.braket.aws_dm1_device import PlanqkAwsDm1Device
from planqk.braket.aws_sv1_device import PlanqkAwsSv1Device
from planqk.braket.ionq_aria_device import PlanqkAwsIonqAriaDevice
from planqk.braket.iqm_garnet_device import PlanqkAwsIqmGarnetDevice
from planqk.braket.planqk_quantum_task import PlanqkAwsQuantumTask
from planqk.braket.quera_aquila_device import PlanqkQueraAquilaDevice
from planqk.client.backend_dtos import BackendDto, BackendStateInfosDto
from planqk.client.client import _PlanqkClient
from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Provider, Job_Input_Format, PlanqkSdkProvider
from planqk.qiskit.providers.aws.aws_backend import PlanqkAwsBackend
from planqk.qiskit.providers.aws.aws_iqm_garnet_backend import PlanqkAwsIqmGarnetBackend
from planqk.qiskit.providers.aws.aws_qiskit_job import PlanqkAwsQiskitJob
from planqk.qiskit.providers.azure.azure_qiskit_job import PlanqkAzureQiskitJob
from planqk.qiskit.providers.azure.ionq_backend import PlanqkAzureIonqBackend
from planqk.qiskit.providers.qryd.qryd_qiskit_job import PlanqkQrydQiskitJob
from tests.testobjects.client.planqk_client_mock import PlanqkClientMock
from tests.util import load_json


class _BackendTestObjects:
    class _BackendTestDtos:
        @staticmethod
        def state_online() -> BackendStateInfosDto:
            return BackendStateInfosDto.model_validate({"status": "ONLINE"})

        @staticmethod
        def state_paused() -> BackendStateInfosDto:
            return BackendStateInfosDto.model_validate({"status": "PAUSED"})

        @staticmethod
        def state_offline() -> BackendStateInfosDto:
            return BackendStateInfosDto.model_validate({"status": "OFFLINE"})

        @staticmethod
        def aws_quera_backend() -> BackendDto:
            return BackendDto(**load_json('aws/data/quera-aquila-backend.json'))

        @staticmethod
        def aws_sv1_backend() -> BackendDto:
            return BackendDto(**load_json('aws/data/aws-sv1-backend.json'))

        @staticmethod
        def aws_ionq_aria_backend() -> BackendDto:
            return BackendDto(**load_json('aws/data/ionq-aria-backend.json'))

        @staticmethod
        def aws_iqm_garnet_backend() -> BackendDto:
            return BackendDto(**load_json('aws/data/iqm-garnet-backend.json'))

        @staticmethod
        def azure_ionq_sim_backend() -> BackendDto:
            return BackendDto(**load_json('azure/data/ionq-sim-backend.json'))

    class _BackendTestConfigs:
        @staticmethod
        def quera_aquila_config() -> Dict[str, Any]:
            return load_json('aws/data/quera-aquila-config.json')

        @staticmethod
        def ionq_aria_config() -> Dict[str, Any]:
            return load_json('aws/data/ionq-aria-config.json')

        @staticmethod
        def iqm_garnet_config() -> Dict[str, Any]:
            return load_json('aws/data/iqm-garnet-config.json')

        @staticmethod
        def aws_sv1_config() -> Dict[str, Any]:
            return load_json('aws/data/aws-sv1-config.json')

        @staticmethod
        def aws_dm1_config() -> Dict[str, Any]:
            return load_json('aws/data/aws-dm1-config.json')

    _configs = _BackendTestConfigs()

    @classmethod
    def configs(cls):
        return cls._configs

    class _BackendCalibrations:
        @staticmethod
        def ionq_aria_calibration() -> Dict[str, Any]:
            return load_json('aws/data/ionq-aria-calibration.json')

    _calibrations = _BackendCalibrations()

    @classmethod
    def calibrations(cls):
        return cls._calibrations

    @staticmethod
    def aws_sv1_backend(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsBackend:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/aws-sv1-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsBackend(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_ionq_aria_backend(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsBackend:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/ionq-aria-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsBackend(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_iqm_garnet_backend(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsBackend:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/iqm-garnet-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsIqmGarnetBackend(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_quera_device(client: _PlanqkClient = None) -> PlanqkQueraAquilaDevice:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/quera-aquila-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkQueraAquilaDevice(planqk_client=client, **{'backend_info': backend_dto})

    @staticmethod
    def azure_ionq_sim_backend(client: _PlanqkClient = None, **kwargs) -> PlanqkAzureIonqBackend:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('azure/data/ionq-sim-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAzureIonqBackend(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def qryd_square_sim_backend(client: _PlanqkClient = None, **kwargs) -> PlanqkAzureIonqBackend:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('qryd/data/qryd-square-sim-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAzureIonqBackend(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_ionq_aria_device(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsIonqAriaDevice:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/ionq-aria-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsIonqAriaDevice(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_iqm_garnet_device(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsIqmGarnetDevice:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/iqm-garnet-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsIqmGarnetDevice(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_sv1_device(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsSv1Device:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/aws-sv1-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsSv1Device(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    @staticmethod
    def aws_dm1_device(client: _PlanqkClient = None, **kwargs) -> PlanqkAwsDm1Device:
        client = client if client is not None else PlanqkClientMock()
        backend_dto = BackendDto(**load_json('aws/data/aws-dm1-backend.json'))
        backend_dto.status = BackendStateInfosDto.model_validate({"status": "ONLINE"})
        return PlanqkAwsDm1Device(planqk_client=client, **{'backend_info': backend_dto}, **kwargs)

    _dtos = _BackendTestDtos()

    @classmethod
    def dtos(cls):
        return cls._dtos


class _JobTestObjects:
    class _JobTestDtos:
        @staticmethod
        def aws_sv1_job() -> JobDto:
            return JobDto(
                provider=Provider.AWS,
                shots=3,
                backend_id='aws.sim.sv1',
                id='b1b1e047-89a4-4a21-99f0-83779b9ccf78',
                provider_job_id='arn:braket:braket:us-east-1:750565748698:quantum-task/06222c79-904f-4be9-bdec-d738119607fb',
                input_params={'disable_qubit_rewiring': False},
                error_data=None,
                started_at='2024-08-09 07:38:13',
                created_at='2024-08-09 07:38:12',
                ended_at='2024-08-09 07:38:15',
                name=None,
                tags=None,
                input=None,
                input_format=Job_Input_Format.BRAKET_OPEN_QASM_V3,
                session_id=None,
                sdk_provider=PlanqkSdkProvider.QISKIT
            )

        @staticmethod
        def aws_ionq_aria_job() -> JobDto:
            return JobDto(
                provider=Provider.AWS,
                shots=3,
                backend_id='aws.ionq.aria',
                id='b1b1e047-89a4-4a21-99f0-83779b9ccf78',
                provider_job_id='arn:braket:braket:us-east-1:750565748698:quantum-task/06222c79-904f-4be9-bdec-d738119607fb',
                input_params={'disable_qubit_rewiring': False},
                error_data=None,
                started_at='2024-08-09 07:38:13',
                created_at='2024-08-09 07:38:12',
                ended_at='2024-08-09 07:38:15',
                input="OPENQASM 3.0;\n"
                      "bit[3] b;\n"
                      "qubit[3] q;\n"
                      "h q[0];\n"
                      "cnot q[0], q[1];\n"
                      "cnot q[1], q[2];\n"
                      "b[0] = measure q[0];\n"
                      "b[1] = measure q[1];\n"
                      "b[2] = measure q[2];",
                name=None,
                tags=None,
                input_format=Job_Input_Format.BRAKET_OPEN_QASM_V3,
                session_id=None,
                sdk_provider=PlanqkSdkProvider.QISKIT
            )

        @staticmethod
        def aws_quera_task() -> JobDto:
            return JobDto(
                provider=Provider.AWS,
                shots=1000,
                backend_id='aws.quera.aquila',
                id='12b1e047-1112-3322-11f0-83779b9ccf99',
                provider_job_id='arn:braket:braket:us-east-1:999565748700:quantum-task/08222c80-904f-55e9-baac-a23811960700',
                input_params={},
                error_data=None,
                started_at='2024-08-12 08:40:03',
                created_at='2024-08-12 08:32:30',
                ended_at='2024-08-12 08:33:15',
                name=None,
                tags=None,
                input=None,
                input_format=Job_Input_Format.BRAKET_AHS_PROGRAM,
                session_id=None,
                sdk_provider=PlanqkSdkProvider.BRAKET
            )

        @staticmethod
        def aws_sv1_task() -> JobDto:
            return JobDto(
                provider=Provider.AWS,
                shots=3,
                backend_id='aws.sim.sv1',
                id='0221e047-1112-3322-11f0-83779b9cc100',
                provider_job_id='arn:braket:braket:us-east-1:999565748700:quantum-task/11222c80-904f-55e9-baac-a23811960999',
                input_params={},
                error_data=None,
                started_at='2024-01-11 00:40:00',
                created_at='2024-01-11 00:39:00',
                ended_at='2024-08-12 08:40:20',
                name=None,
                tags=None,
                input=None,
                input_format=Job_Input_Format.BRAKET_OPEN_QASM_V3,
                session_id=None,
                sdk_provider=PlanqkSdkProvider.BRAKET
            )

        @staticmethod
        def azure_ionq_sim_job() -> JobDto:
            return JobDto(
                provider=Provider.AZURE,
                shots=3,
                backend_id='azure.ionq.sim',
                id='94aa3705-5e24-48be-a145-e3fce705f243',
                provider_job_id='50428339-f847-4a6d-8b93-40a9bb907240',
                input_params={'qubits': '3', 'memory': 'True'},
                error_data=None,
                started_at='2024-08-10 12:38:13',
                created_at='2024-08-10 12:38:12',
                ended_at='2024-08-10 12:38:15',
                name=None,
                tags=None,
                input=None,
                input_format=Job_Input_Format.IONQ_CIRCUIT_V1,
                session_id=None,
                sdk_provider=PlanqkSdkProvider.QISKIT
            )

        @staticmethod
        def qryd_square_sim_job() -> JobDto:
            return JobDto(
                provider=Provider.QRYD,
                shots=1024,
                backend_id='qryd.sim.square',
                id='6e4a68ca-f631-40cc-ac27-b7012ab30f7f',
                provider_job_id='347750',
                input_params={},
                error_data=None,
                started_at='2024-10-22 18:38:13',
                created_at='2024-10-22 18:38:12',
                ended_at='2024-10-22 18:38:15',
                name=None,
                tags=None,
                input=None,
                input_format=Job_Input_Format.QOQO,
                session_id=None,
                sdk_provider=PlanqkSdkProvider.QISKIT
            )

        @staticmethod
        def valid_job_dto():
            return JobDto.model_validate({"id": "123", "shots": 1, "provider": "PROVIDER.AWS"})

    class _JobTestInputs:
        @staticmethod
        def qiskit_3_ghz() -> QuantumCircuit:
            circuit = QuantumCircuit(3, 3)
            circuit.name = "Qiskit Sample - 3-qubit GHZ input"
            circuit.h(0)
            circuit.cx(0, 1)
            circuit.cx(1, 2)
            circuit.measure([0, 1, 2], [0, 1, 2])
            return circuit

        @staticmethod
        def braket_3_ghz() -> Circuit:
            circuit = Circuit()
            circuit.h(0)
            circuit.cnot(0, 1)
            circuit.cnot(1, 2)
            return circuit

        @staticmethod
        def ahs_program() -> AnalogHamiltonianSimulation:
            separation = 5e-6
            block_separation = 15e-6
            k_max = 5
            m_max = 5

            register = AtomArrangement()
            for k in range(k_max):
                for m in range(m_max):
                    register.add((block_separation * m, block_separation * k + separation / np.sqrt(3)))
                    register.add((block_separation * m - separation / 2, block_separation * k - separation / (2 * np.sqrt(3))))
                    register.add((block_separation * m + separation / 2, block_separation * k - separation / (2 * np.sqrt(3))))

            # Driving
            omega_const = 1.5e7  # rad / s
            rabi_pulse_area = np.pi / np.sqrt(3)  # rad
            rydberg_global = {'rabiFrequencySlewRateMax': Decimal('400000000000000.0'), 'timeDeltaMin': Decimal('5E-8')}
            omega_slew_rate_max = float(rydberg_global['rabiFrequencySlewRateMax'])  # rad/s^2
            time_separation_min = float(rydberg_global['timeDeltaMin'])  # s

            amplitude = TimeSeries.trapezoidal_signal(rabi_pulse_area, omega_const, 0.99 * omega_slew_rate_max,
                                                      time_separation_min=time_separation_min)

            detuning = TimeSeries.constant_like(amplitude, 0.0)
            phase = TimeSeries.constant_like(amplitude, 0.0)

            drive = DrivingField(
                amplitude=amplitude,
                detuning=detuning,
                phase=phase
            )

            return AnalogHamiltonianSimulation(
                hamiltonian=drive,
                register=register
            )

    class _JobTestResults:
        @staticmethod
        def ahs_result() -> AnalogHamiltonianSimulationQuantumTaskResult:
            return load_json("aws/data/quera-aquila-ahs-result.json")

        @staticmethod
        def qiskit_measurement_result():
            return load_json("aws/data/qiskit_measurement_result.json")

        @staticmethod
        def aws_sv1_result():
            return load_json("aws/data/aws-sv1-result.json")

        @staticmethod
        def aws_ionq_aria_result():
            return load_json("aws/data/aws-ionq-aria-result.json")

        @staticmethod
        def azure_ionq_sim_result():
            return load_json("azure/data/ionq-sim-result.json")

        @staticmethod
        def qryd_square_sim_result():
            return load_json("qryd/data/qryd-sim-result.json")

        @staticmethod
        def error_result() -> dict:
            return {'code': 'InsufficientResources', 'message': 'Too many qubits requested'}

    _dtos = _JobTestDtos()
    _inputs = _JobTestInputs()
    _results = _JobTestResults()

    @classmethod
    def dtos(cls):
        return cls._dtos

    @classmethod
    def inputs(cls):
        return cls._inputs

    @classmethod
    def results(cls):
        return cls._results

    @classmethod
    def aws_quera_task(cls, client: _PlanqkClient = None) -> PlanqkAwsQuantumTask:
        task_dto = cls._dtos.aws_quera_task()
        client = client if client is not None else PlanqkClientMock()
        return PlanqkAwsQuantumTask(_backend=None, task_id=task_dto.id, _job_details=task_dto, _client=client)

    @classmethod
    def aws_sv1_task(cls, client: _PlanqkClient = None) -> PlanqkAwsQiskitJob:
        task_dto = cls._dtos.aws_sv1_task()
        client = client if client is not None else PlanqkClientMock()
        return PlanqkAwsQuantumTask(_backend=None, task_id=task_dto.id, _job_details=task_dto, _client=client)


    @classmethod
    def aws_sv1_job(cls, client: _PlanqkClient = None) -> PlanqkAwsQiskitJob:
        job = cls._dtos.aws_sv1_job()
        client = client if client is not None else PlanqkClientMock()
        return PlanqkAwsQiskitJob(backend=_BackendTestObjects.aws_sv1_backend(client), job_id=job.id, job_details=job, planqk_client=client)

    @classmethod
    def aws_ionq_aria_job(cls, client: _PlanqkClient = None) -> PlanqkAwsQiskitJob:
        job = cls._dtos.aws_ionq_aria_job()
        client = client if client is not None else PlanqkClientMock()
        return PlanqkAwsQiskitJob(backend=_BackendTestObjects.aws_ionq_aria_backend(client), job_id=job.id, job_details=job, planqk_client=client)

    @classmethod
    def azure_ionq_sim_job(cls, client: _PlanqkClient = None) -> PlanqkAzureQiskitJob:
        job = cls._dtos.azure_ionq_sim_job()
        client = client if client is not None else PlanqkClientMock()
        return PlanqkAzureQiskitJob(backend=_BackendTestObjects.azure_ionq_sim_backend(client), job_id=job.id, job_details=job, planqk_client=client)

    @classmethod
    def qryd_square_sim_job(cls, client: _PlanqkClient = None) -> PlanqkAzureQiskitJob:
        job = cls._dtos.qryd_square_sim_job()
        client = client if client is not None else PlanqkClientMock()
        return PlanqkQrydQiskitJob(backend=_BackendTestObjects.azure_ionq_sim_backend(client), job_id=job.id, job_details=job, planqk_client=client)


class TestObjects:
    _job_objects = _JobTestObjects()
    _backend_objects = _BackendTestObjects()

    @classmethod
    def jobs(cls):
        return cls._job_objects

    @classmethod
    def backends(cls):
        return cls._backend_objects
