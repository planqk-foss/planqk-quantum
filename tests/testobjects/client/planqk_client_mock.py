from typing import Optional, List, Dict, Any, Callable

from planqk.client.backend_dtos import BackendStateInfosDto, BackendDto
from planqk.client.client import _PlanqkClient
from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Job_Status


class PlanqkClientMock(_PlanqkClient):
    def __init__(self, access_token: Optional[str] = None, organization_id: Optional[str] = None):
        """
        Initialize the mock PlanqkClient with optional access token and organization ID.
        """
        super().__init__(access_token, organization_id)

        self._credentials = None
        self._organization_id = organization_id

    def get_credentials(self):
        """
        Mock method to get credentials.
        Returns:
            None
        """
        return self._credentials

    def set_organization_id(self, organization_id: str):
        """
        Mock method to set the organization ID.
        Args:
            organization_id (str): The organization ID to set.
        """
        self._organization_id = organization_id

    def perform_request(
            self,
            request_func: Callable[..., Any],
            url: str,
            params: Optional[Dict[str, Any]] = None,
            data: Optional[Dict[str, Any]] = None,
            headers: Optional[Dict[str, Any]] = None
    ) -> Optional[Any]:
        """
        Mock method to perform a request.
        Args:
            request_func (Callable): The request function to mock.
            url (str): The URL for the request.
            params (dict, optional): Query parameters.
            data (dict, optional): Request payload.
            headers (dict, optional): Request headers.
        Returns:
            None
        """
        return None

    def get_backends(self) -> List[BackendDto]:
        """
        Mock method to get a list of backends.
        Returns:
            List[BackendDto]: An empty list.
        """
        return []

    def get_backend(self, backend_id: str) -> Optional[BackendDto]:
        """
        Mock method to get a specific backend by ID.
        Args:
            backend_id (str): The ID of the backend.
        Returns:
            BackendDto or None: None since it's a mock.
        """
        return None

    def get_backend_state(self, backend_id: str) -> Optional[BackendStateInfosDto]:
        """
        Mock method to get the state of a specific backend.
        Args:
            backend_id (str): The ID of the backend.
        Returns:
            BackendStateInfosDto or None: None since it's a mock.
        """
        return None

    def get_backend_config(self, backend_id: str) -> Dict[str, Any]:
        """
        Mock method to get the configuration of a specific backend.
        Args:
            backend_id (str): The ID of the backend.
        Returns:
            dict: An empty dictionary.
        """
        return {}

    def submit_job(self, job: JobDto) -> Optional[JobDto]:
        """
        Mock method to submit a job.
        Args:
            job (JobDto): The job to submit.
        Returns:
            JobDto or None: None since it's a mock.
        """
        return None

    def get_job(self, job_id: str) -> Optional[JobDto]:
        """
        Mock method to get a specific job by ID.
        Args:
            job_id (str): The ID of the job.
        Returns:
            JobDto or None: None since it's a mock.
        """
        return None

    def get_jobs(self) -> List[JobDto]:
        """
        Mock method to get a list of jobs.
        Returns:
            List[JobDto]: An empty list.
        """
        return []

    def get_job_status(self, job_id: str) -> Optional[Job_Status]:
        """
        Mock method to get the status of a specific job.
        Args:
            job_id (str): The ID of the job.
        Returns:
            Job_Status or None: None since it's a mock.
        """
        return None

    def get_job_result(self, job_id: str) -> Dict[str, Any]:
        """
        Mock method to get the result of a specific job.
        Args:
            job_id (str): The ID of the job.
        Returns:
            dict: An empty dictionary.
        """
        return {}

    def cancel_job(self, job_id: str) -> None:
        """
        Mock method to cancel a specific job.
        Args:
            job_id (str): The ID of the job.
        """
        pass
