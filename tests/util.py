import json
import os


def load_json(testobjects_sub_path):
    filepath = os.path.join(os.path.dirname(__file__), 'testobjects', testobjects_sub_path)
    with open(filepath, 'r') as file:
        return json.load(file)
