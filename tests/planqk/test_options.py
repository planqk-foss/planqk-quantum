import unittest

from planqk.qiskit.options import OptionsV2
from tests.testobjects.test_objects import TestObjects


class OptionsTestSuite(unittest.TestCase):

    def test_options_patch(self):
        options = OptionsV2(foo="bar")

        self.assertEqual(options["foo"], "bar")
        self.assertEqual(options.data, ["foo"])

        options["foo"] = "baz"
        self.assertEqual(options["foo"], "baz")

        options.update_options(foo="qux")
        self.assertEqual(options["foo"], "qux")

    def test_set_backend_options(self):
        backend = TestObjects.backends().azure_ionq_sim_backend(gateset='zz')
        self.assertEqual(backend.options["gateset"], "zz")
