import unittest
from unittest.mock import patch, MagicMock

from planqk.client.client import _PlanqkClient
from planqk.client.job_dtos import JobDto
from planqk.qiskit import PlanqkQiskitJob
from planqk.qiskit.providers.aws.aws_qiskit_job import PlanqkAwsQiskitJob
from tests.testobjects.test_objects import TestObjects


class JobTestSuite(unittest.TestCase):

    def test_job_should_have_queue_position_attribute(self):
        backend = TestObjects.backends().aws_sv1_backend()
        job = PlanqkQiskitJob(backend, job_id="1337", job_details=JobDto(**{"provider": "AWS"}), planqk_client=_PlanqkClient())

        queue_position = job.queue_position()

        self.assertIsNone(queue_position)

    def test_should_get_job_id(self):
        backend = TestObjects.backends().aws_sv1_backend()
        job = PlanqkQiskitJob(backend, job_id="1337", job_details=JobDto(**{"provider": "AZURE"}), planqk_client=_PlanqkClient())

        self.assertIsNone(job.id)
        self.assertIsNone(job.job_id())

    @patch("planqk.client.client._PlanqkClient")
    def test_should_return_calibration(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value

        calibration_mock = TestObjects.backends().calibrations().ionq_aria_calibration()
        client_mock.get_job_calibration.return_value = calibration_mock

        backend = TestObjects.backends().aws_ionq_aria_backend(client_mock)

        # When
        job_id = "test_job_id"
        job = PlanqkAwsQiskitJob(backend, job_id=job_id, job_details=JobDto(**{"id": job_id, "provider": "AWS"}), planqk_client=client_mock)
        result = job.calibration()

        # Then
        client_mock.get_job_calibration.assert_called_once_with(job_id)
        self.assertEqual(result, calibration_mock)


