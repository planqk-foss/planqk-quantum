import unittest
from unittest.mock import patch, MagicMock

from tests.testobjects.test_objects import TestObjects


class PlanqkBackendTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_calibration(self, client_mock_class: MagicMock):
        client_mock = client_mock_class.return_value

        calibration_mock = TestObjects.backends().calibrations().ionq_aria_calibration()
        client_mock.get_backend_calibration.return_value = calibration_mock

        backend = TestObjects.backends().aws_ionq_aria_backend(client_mock)

        # When
        result = backend.calibration()

        # Then
        client_mock.get_backend_calibration.assert_called_once_with("aws.ionq.aria")
        self.assertEqual(result, calibration_mock)
        

