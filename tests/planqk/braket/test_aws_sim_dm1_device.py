import unittest
from unittest.mock import patch, MagicMock

from braket.aws import AwsDeviceType
from braket.device_schema.simulators import GateModelSimulatorDeviceCapabilities

from tests.testobjects.test_objects import TestObjects


class AwsDm1DeviceTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_get_device_properties(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_dm1_device(client_mock)
        client_mock.get_backend_config.return_value = TestObjects.backends().configs().aws_dm1_config()

        # Then
        self.assertEqual("dm1", device.name)
        self.assertEqual("Amazon Braket", device.provider_name)
        self.assertEqual(AwsDeviceType.SIMULATOR, device.type)
        self.assertIsInstance(device.properties, GateModelSimulatorDeviceCapabilities)
        self.assertIsNone(device.topology_graph)
        self.assertEqual({}, device.frames)
        self.assertIsNone(device.gate_calibrations)
