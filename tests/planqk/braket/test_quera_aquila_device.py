import unittest
from unittest.mock import patch, MagicMock

from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Job_Input_Format, PlanqkSdkProvider
from tests.testobjects.test_objects import TestObjects
from tests.util import load_json


class QueraAquilaDeviceTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_get_device_properties(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_quera_device(client_mock)
        client_mock.get_backend_config.return_value = TestObjects.backends().configs().quera_aquila_config()

        # When

        # Then
        self.assertEqual("Aquila", device.name)
        self.assertEqual("QuEra", device.provider_name)

        properties = device.properties
        self.assertIsNotNone(properties.Config)
        self.assertIsNotNone(properties.action)
        self.assertIsNotNone(properties.service)
        self.assertIsNotNone(properties.paradigm.Config)
        self.assertIsNotNone(properties.paradigm.lattice)
        self.assertIsNotNone(properties.paradigm.performance)
        self.assertIsNotNone(properties.paradigm.rydberg.rydbergGlobal)

    @patch("planqk.client.client._PlanqkClient")
    def test_run_task(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        backend = TestObjects.backends().aws_quera_device(client_mock)
        ahs_program = TestObjects.jobs().inputs().ahs_program()

        # When
        backend.run(task_specification=ahs_program, shots=1000)

        ahs_program_json = load_json('aws/data/quera-aquila-ahs-program.json')

        # Then
        exp_job = JobDto(
            provider='AWS',
            shots=1000,
            backend_id='aws.quera.aquila',
            id=None,
            provider_job_id=None,
            session_id=None,
            input={'ahs_program' : ahs_program_json},
            input_format=Job_Input_Format.BRAKET_AHS_PROGRAM,
            input_params={},
            started_at=None,
            created_at=None,
            ended_at=None,
            name=None,
            tags=None,
            sdk_provider=PlanqkSdkProvider.BRAKET
        )

        client_mock.submit_job.assert_called_once_with(exp_job)
