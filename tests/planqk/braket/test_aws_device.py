import datetime
import unittest
from unittest.mock import patch, MagicMock

from planqk.client.model_enums import BackendType
from tests.testobjects.test_objects import TestObjects


class PlanqkAwsDeviceTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_refresh_metadata(self, client_mock_class: MagicMock):
        # Given backend and updated backend
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_quera_device(client_mock)
        updated_metadata = TestObjects.backends().dtos().aws_quera_backend()
        current_date = datetime.date.today()
        updated_metadata.updated_at = current_date
        client_mock.get_backend.return_value = updated_metadata

        # When
        device.refresh_metadata()

        # Then
        self.assertEqual(current_date, device.backend_info.updated_at)

    @patch("planqk.client.client._PlanqkClient")
    def test_type(self, client_mock_class: MagicMock):
        # Given backend and updated backend
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_quera_device(client_mock)

        # When
        type = device.type

        # Then
        self.assertEqual(BackendType.QPU.name, type)

    @patch("planqk.client.client._PlanqkClient")
    def test_status_paused_mapped_to_online_not_available(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_quera_device(client_mock)
        client_mock.get_backend_state.return_value = TestObjects.backends().dtos().state_paused()

        # When / Then
        self.assertEqual("ONLINE", device.status)
        self.assertEqual(False, device.is_available)

    @patch("planqk.client.client._PlanqkClient")
    def test_status_offline_mapped_to_offline(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        backend = TestObjects.backends().aws_quera_device(client_mock)
        client_mock.get_backend_state.return_value = TestObjects.backends().dtos().state_offline()

        # When / Then
        self.assertEqual("OFFLINE", backend.status)
        self.assertEqual(False, backend.is_available)

    @patch("planqk.client.client._PlanqkClient")
    def test_is_available(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_quera_device(client_mock)
        client_mock.get_backend_state.return_value = TestObjects.backends().dtos().state_online()

        # When / Then
        self.assertEqual("ONLINE", device.status)
        self.assertEqual(True, device.is_available)

    def test_gate_calibration_none(self):
        # Given
        device = TestObjects.backends().aws_quera_device()

        # When / Then
        self.assertIsNone(device.gate_calibrations)

    def test_session_not_supported(self):
        # Given
        device = TestObjects.backends().aws_quera_device()

        # When
        with self.assertRaises(NotImplementedError) as context:
            device.aws_session

        # Then
        self.assertEqual(
            str(context.exception),
            "This function is not supported by the SDK."
        )

    @patch("planqk.client.client._PlanqkClient")
    def test_get_device_properties(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_quera_device(client_mock)
        client_mock.get_backend_config.return_value = TestObjects.backends().configs().quera_aquila_config()

        # When
        properties = device.properties

        # Then
        self.assertIsNotNone(properties.Config)
        self.assertIsNotNone(properties.action)
        self.assertIsNotNone(properties.service)
