import unittest
from unittest.mock import patch, MagicMock

from planqk.braket.planqk_quantum_task import PlanqkAwsQuantumTask
from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Job_Input_Format, PlanqkSdkProvider
from tests.testobjects.test_objects import TestObjects


class GateBasedDeviceTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_run_task(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_ionq_aria_device(client_mock)
        circuit = TestObjects.jobs().inputs().braket_3_ghz()

        exp_job = JobDto(
            provider='AWS',
            shots=1000,
            backend_id='aws.ionq.aria',
            id=None,
            provider_job_id=None,
            session_id=None,
            input="OPENQASM 3.0;\n"
                  "bit[3] b;\n"
                  "qubit[3] q;\n"
                  "h q[0];\n"
                  "cnot q[0], q[1];\n"
                  "cnot q[1], q[2];\n"
                  "b[0] = measure q[0];\n"
                  "b[1] = measure q[1];\n"
                  "b[2] = measure q[2];",
            input_format=Job_Input_Format.BRAKET_OPEN_QASM_V3,
            input_params={'disable_qubit_rewiring': False},
            started_at=None,
            created_at=None,
            ended_at=None,
            name=None,
            tags=None,
            sdk_provider=PlanqkSdkProvider.BRAKET
        )

        client_mock.submit_job.return_value = exp_job

        # When
        task = device.run(task_specification=circuit, shots=1000)

        self.assertIsInstance(task, PlanqkAwsQuantumTask)

        # Then
        client_mock.submit_job.assert_called_once_with(exp_job)
