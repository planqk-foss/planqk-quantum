import unittest
from unittest.mock import patch, MagicMock

from braket.aws import AwsDeviceType
from braket.device_schema.ionq import IonqDeviceCapabilities
from tests.testobjects.test_objects import TestObjects


class IonqAriaDeviceTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_get_device_properties(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_ionq_aria_device(client_mock)
        client_mock.get_backend_config.return_value = TestObjects.backends().configs().ionq_aria_config()

        # Then
        self.assertEqual("Aria 1", device.name)
        self.assertEqual("IonQ", device.provider_name)
        self.assertEqual(AwsDeviceType.QPU, device.type)
        self.assertIsInstance(device.properties, IonqDeviceCapabilities)
        self.assertEqual(25, device.topology_graph.number_of_nodes())
        self.assertEqual(600, device.topology_graph.number_of_edges())
        self.assertEqual({}, device.frames)
        self.assertIsNone(device.gate_calibrations)
