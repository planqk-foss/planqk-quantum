import unittest
from unittest.mock import patch, MagicMock

from braket.aws import AwsDeviceType
from braket.device_schema.iqm import IqmDeviceCapabilities
from tests.testobjects.test_objects import TestObjects


class IqmGarnetDeviceTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_get_device_properties(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        device = TestObjects.backends().aws_iqm_garnet_device(client_mock)
        client_mock.get_backend_config.return_value = TestObjects.backends().configs().iqm_garnet_config()

        # Then
        self.assertEqual("Garnet", device.name)
        self.assertEqual("IQM", device.provider_name)
        self.assertEqual(AwsDeviceType.QPU, device.type)
        self.assertIsInstance(device.properties, IqmDeviceCapabilities)
        self.assertEqual(20, device.topology_graph.number_of_nodes())
        self.assertEqual(30, device.topology_graph.number_of_edges())
        self.assertEqual({}, device.frames)
        self.assertIsNone(device.gate_calibrations)
