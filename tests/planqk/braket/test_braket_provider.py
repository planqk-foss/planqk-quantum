import unittest
from unittest.mock import patch, MagicMock, Mock

from planqk import PlanqkBraketProvider
from planqk.braket import PlanqkQueraAquilaDevice, PlanqkAwsIqmGarnetDevice
from planqk.braket.ionq_aria_device import PlanqkAwsIonqAriaDevice
from planqk.exceptions import BackendNotFoundError, PlanqkClientError
from tests.testobjects.test_objects import TestObjects


class TestPlanqkBraketProvider(unittest.TestCase):

    def test_list_supported_devices(self):
        devices = PlanqkBraketProvider().devices()
        self.assertIsNotNone(devices)
        self.assertIsInstance(devices, list)
        self.assertEqual(7, len(devices))

    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_should_get_ionq_device(self, get_backend_mock):
        get_backend_mock.return_value = TestObjects.backends().dtos().aws_ionq_aria_backend()

        device = PlanqkBraketProvider().get_device("aws.ionq.aria")
        self.assertIsNotNone(device)
        self.assertIsInstance(device, PlanqkAwsIonqAriaDevice)

    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_get_aws_aquila_device(self, get_backend_mock):
        get_backend_mock.return_value = TestObjects.backends().dtos().aws_quera_backend()

        device = PlanqkBraketProvider().get_device("aws.quera.aquila")
        self.assertIsNotNone(device)
        self.assertIsInstance(device, PlanqkQueraAquilaDevice)

    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_get_aws_aquila_device(self, get_backend_mock):
        get_backend_mock.return_value = TestObjects.backends().dtos().aws_iqm_garnet_backend()

        device = PlanqkBraketProvider().get_device("aws.iqm.garnet")
        self.assertIsNotNone(device)
        self.assertIsInstance(device, PlanqkAwsIqmGarnetDevice)

    @patch("planqk.client.client._PlanqkClient")
    def test_should_raise_backend_not_found_when_backend_id_invalid(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        mock_response = Mock()
        mock_response.status_code = 404
        mock_response.text = "{\"status\": 404,  \"error_message\": \"The backend with id 123 could not be found\"}"
        client_mock.get_backend.side_effect = PlanqkClientError(mock_response)

        # When / Then
        with self.assertRaises(BackendNotFoundError):
            backend_id = "invalid.id"
            PlanqkBraketProvider(_client=client_mock).get_device(backend_id=backend_id)