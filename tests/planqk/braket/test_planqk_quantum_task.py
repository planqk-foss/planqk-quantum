import unittest
from unittest.mock import patch, MagicMock

import numpy as np

from planqk.braket import PlanqkAwsQuantumTask
from planqk.client.model_enums import Job_Status
from planqk.exceptions import PlanqkError
from tests.testobjects.test_objects import TestObjects


class PlanqkQuantumTaskTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_get_task(self, client_mock_class: MagicMock):
        client_mock = client_mock_class.return_value
        quera_task_dto = TestObjects.jobs().dtos().aws_quera_task()
        client_mock.get_job.return_value = quera_task_dto

        # When
        task = PlanqkAwsQuantumTask(task_id=quera_task_dto.id, _client=client_mock)

        # Then
        client_mock.get_job.assert_called_once_with(quera_task_dto.id)
        self.assertEqual(quera_task_dto.id, task.id)

    @patch("planqk.client.client._PlanqkClient")
    def test_raise_error_when_getting_task_not_executed_on_aws_backend(self, client_mock_class: MagicMock):
        # Given job performed on Azure
        client_mock = client_mock_class.return_value
        qiskit_job_dto = TestObjects.jobs().dtos().azure_ionq_sim_job()

        # When
        with self.assertRaises(PlanqkError):
            PlanqkAwsQuantumTask(task_id=qiskit_job_dto.id, _client=client_mock)

    def test_create_method_not_supported(self):
        with self.assertRaises(NotImplementedError):
            PlanqkAwsQuantumTask.create(aws_session=None, device_arn=None, task_specification=None, s3_destination_folder=None, shots=100)

    def test_metadata_method_not_supported(self):
        task = TestObjects.jobs().aws_quera_task()
        with self.assertRaises(NotImplementedError):
            task.metadata()

    @patch("planqk.client.client._PlanqkClient")
    def test_get_task_status(self, client_mock_class: MagicMock):
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_quera_task(client_mock)
        client_mock.get_job_status.return_value = Job_Status.PENDING

        # When / Then
        self.assertEqual("QUEUED", task.state())

        client_mock.get_job_status.return_value = Job_Status.COMPLETED
        self.assertEqual("COMPLETED", task.state())

    @patch("planqk.client.client._PlanqkClient")
    def test_get_cached_task_state(self, client_mock_class: MagicMock):
        # Given task in Queued
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_quera_task(client_mock)
        client_mock.get_job_status.return_value = Job_Status.PENDING
        task.state()

        # When do not fetch state from client Queued state should be returned
        client_mock.get_job_status.return_value = Job_Status.COMPLETED
        self.assertEqual("QUEUED", task.state(use_cached_value=True))

    def test_get_task_id(self):
        # Given
        task = TestObjects.jobs().aws_quera_task()

        # When / Then
        self.assertIsNotNone(task.id)

    @patch("planqk.client.client._PlanqkClient")
    def test_cancel_task(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_quera_task(client_mock)

        # When
        task.cancel()

        # Then
        client_mock.cancel_job.assert_called_once_with(task.id)

    @patch("planqk.client.client._PlanqkClient")
    def test_get_ahs_result(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_quera_task(client_mock)
        client_mock.get_job_result.return_value = TestObjects.jobs().results().ahs_result()
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        result = task.result()

        # Then
        self.assertIsNotNone(result)
        self.assertEqual(1000, len(result.measurements))

    @patch("planqk.client.client._PlanqkClient")
    def test_get_gate_based_result(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_sv1_task(client_mock)
        client_mock.get_job_result.return_value = TestObjects.jobs().results().aws_sv1_result()
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        result = task.result()

        # Then
        self.assertIsNotNone(result)
        exp_measurements = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
        self.assertTrue(np.array_equal(exp_measurements, result.measurements))
        self.assertEqual([0, 1, 2], result.measured_qubits)

    @patch("planqk.client.client._PlanqkClient")
    def test_raise_error_when_result_invalid_result(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_sv1_task(client_mock)
        client_mock.get_job_result.return_value = "invalid_result"
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        with self.assertRaises(PlanqkError):
            task.result()
