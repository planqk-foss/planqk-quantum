import unittest
from unittest.mock import patch, MagicMock, Mock

from requests import HTTPError

from planqk.client.backend_dtos import BackendDto
from planqk.client.client import _PlanqkClient, HEADER_CLOUD_TRACE_CTX
from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Provider
from planqk.exceptions import PlanqkClientError, InvalidAccessTokenError
from tests.planqk.client_mocks import rigetti_mock, oqc_lucy_mock, job_mock, job_result_mock
from tests.testobjects.test_objects import TestObjects


class PlanqkClientTestSuite(unittest.TestCase):

    def setUp(self):
        self._client = _PlanqkClient()
        self._client._get_default_headers = MagicMock(
            return_value={"x-auth-token": "test_token", HEADER_CLOUD_TRACE_CTX: "test_trace"})

    @patch("requests.get")
    def test_get_backends(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = [rigetti_mock, oqc_lucy_mock]

        # When
        result = self._client.get_backends()

        # Then
        self.assertEqual(2, len(result))
        self.assert_backend(rigetti_mock, result[0])
        self.assert_backend(oqc_lucy_mock, result[1])

    @patch("requests.get")
    def test_get_backend(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = rigetti_mock

        # When
        result = self._client.get_backend(rigetti_mock["id"])

        # Then
        self.assert_backend(rigetti_mock, result)

    @patch("requests.get")
    def test_get_backend_config(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = TestObjects.backends().configs().ionq_aria_config()

        # When
        result = self._client.get_backend_config("aws.ionq.aria")

        # Then
        self.assertIsInstance(result, dict)
        self.assertIn('service', result)
        self.assertIn('action', result)
        self.assertIn('deviceParameters', result)
        self.assertIn('braketSchemaHeader', result)
        self.assertIn('paradigm', result)
        self.assertIn('provider', result)

    @patch("requests.get")
    def test_get_backend_calibration(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = TestObjects.backends().calibrations().ionq_aria_calibration()

        # When
        result = self._client.get_backend_calibration("aws.ionq.aria")

        # Then
        self.assertIsInstance(result, dict)
        self.assertEqual("aws.ionq.aria", result["backend_id"])
        self.assertEqual("2024-11-28T00:33:09Z", result["calibrated_at"])
        self.assertEqual(100, result["t1"])
        self.assertEqual(1, result["t2"])
        self.assertEqual(0.00002, result["reset_time"])
        self.assertEqual(0.0003, result["readout_time"])
        self.assertEqual(0.9939, result["spam_fidelity"])
        self.assertEqual(0.9998, result["one_qubit_fidelity"])
        self.assertEqual(0.9843, result["two_qubit_fidelity"])
        self.assertEqual(0.000135, result["one_qubit_gate_time"])
        self.assertEqual(0.0006, result["two_qubit_gate_time"])

    @patch("requests.post")
    def test_submit_job(self, mock_post):
        # Give
        mock_post.return_value.status_code = 201
        mock_post.return_value.json.return_value = {"id": "123", "backend_id": rigetti_mock["id"],
                                                    "provider": Provider.AWS.name}

        # When
        job = JobDto(**{"backend_id": rigetti_mock["id"], "provider": Provider.AWS.name})
        job_details = self._client.submit_job(job)

        # Then
        self.assertEqual("123", job_details.id)

    @patch("requests.get")
    def test_get_job(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = job_mock

        # When
        job = self._client.get_job("123")

        # Then
        self.assertEqual(job_mock["backend_id"], job.backend_id)
        self.assertEqual(job_mock["provider"], job.provider.value)
        self.assertEqual(job_mock["shots"], job.shots)
        self.assertEqual(job_mock["id"], job.id)
        self.assertDictEqual(job_mock["input"], job.input)
        self.assertEqual(job_mock["input_format"], job.input_format)
        self.assertDictEqual(job_mock["input_params"], job.input_params)
        self.assertEqual(job_mock["started_at"], job.started_at)
        self.assertEqual(job_mock["created_at"], job.created_at)
        self.assertEqual(job_mock["ended_at"], job.ended_at)
        self.assertEqual(job_mock["name"], job.name)
        self.assertSetEqual(job_mock["tags"], job.tags)

    @patch("requests.get")
    def test_get_jobs_single_page(self, mock_get):
        # Given
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            'content': [
                {
                    'id': '1',
                    'backend_id': 'aws.quera.aquila',
                    'provider': 'AWS',
                    'status': 'COMPLETED',
                    'created_at': '2024-08-26 08:27:24',
                    'started_at': '2024-08-26 08:27:25',
                    'ended_at': '2024-08-26 08:27:27',
                    'shots': 1,
                    'input_format': 'BRAKET_AHS_PROGRAM'
                }
            ],
            'total_pages': 1,
            'last': True
        }
        mock_get.return_value = mock_response

        # When
        jobs = self._client.get_jobs()

        # Then
        self.assertEqual(len(jobs), 1)
        self.assertIsInstance(jobs[0], JobDto)
        self.assertEqual(jobs[0].id, '1')

    @patch("requests.get")
    def test_get_jobs_multiple_pages(self, mock_get):
        # Given
        mock_responses = [
            MagicMock(),
            MagicMock()
        ]
        mock_responses[0].status_code = 200
        mock_responses[0].json.return_value = {
            'content': [
                {
                    'id': '1',
                    'backend_id': 'aws.quera.aquila',
                    'provider': 'AWS',
                    'status': 'COMPLETED',
                    'created_at': '2024-08-26 08:27:24',
                    'started_at': '2024-08-26 08:27:25',
                    'ended_at': '2024-08-26 08:27:27',
                    'shots': 1,
                    'input_format': 'BRAKET_AHS_PROGRAM'
                }
            ],
            'total_pages': 2,
            'last': False
        }
        mock_responses[1].status_code = 200
        mock_responses[1].json.return_value = {
            'content': [
                {
                    'id': '2',
                    'backend_id': 'aws.quera.aquila',
                    'provider': 'AWS',
                    'status': 'COMPLETED',
                    'created_at': '2024-08-26 09:00:00',
                    'started_at': '2024-08-26 09:00:01',
                    'ended_at': '2024-08-26 09:00:02',
                    'shots': 1,
                    'input_format': 'BRAKET_AHS_PROGRAM'
                }
            ],
            'total_pages': 2,
            'last': True
        }

        mock_get.side_effect = mock_responses

        # When
        jobs = self._client.get_jobs()

        # Then
        self.assertEqual(len(jobs), 2)
        self.assertEqual(jobs[0].id, '1')
        self.assertEqual(jobs[1].id, '2')

    @patch("requests.get")
    def test_get_jobs_no_jobs(self, mock_get):
        # Given
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {
            'content': [],
            'total_pages': 1,
            'last': True
        }
        mock_get.return_value = mock_response

        # When
        jobs = self._client.get_jobs()

        # Then
        self.assertEqual(len(jobs), 0)

    @patch("requests.get")
    def test_get_job_result(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = job_result_mock

        # When
        result = self._client.get_job_result("123")

        # Then
        for key, value in result["counts"].items():
            self.assertEqual(job_result_mock["counts"][key], value)

        for i, value in enumerate(result["memory"]):
            self.assertEqual(job_result_mock["memory"][i], value)

    @patch("requests.delete")
    def test_cancel_job(self, mock_delete):
        # Given
        mock_delete.return_value.status_code = 204

        # When
        self._client.cancel_job(job_id="123")

        # No exception means success

    @patch("requests.get")
    def test_get_job_calibration(self, mock_get):
        # Given
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = TestObjects.backends().calibrations().ionq_aria_calibration()

        # When
        result = self._client.get_job_calibration("123")

        # Then
        self.assertIsInstance(result, dict)
        self.assertEqual("aws.ionq.aria", result["backend_id"])
        self.assertEqual("2024-11-28T00:33:09Z", result["calibrated_at"])
        self.assertEqual(100, result["t1"])


    @patch("requests.get")
    def test_invalid_token(self, mock_get):
        # Given
        mock_response = Mock()
        mock_response.status_code = 401
        mock_response.json.return_value = {""}
        mock_response.raise_for_status.side_effect = HTTPError("Error", response=mock_response)

        mock_get.return_value = mock_response
        mock_get.__name__ = "get"

        # When
        with self.assertRaises(InvalidAccessTokenError) as error_response:
            self._client.get_backend("123")

        # Then
        assert "Invalid personal access token provided." in error_response.exception.message

    @patch("requests.get")
    def test_planqk_client_error(self, mock_get):
        # Given
        mock_response = Mock()
        mock_response.status_code = 404
        mock_response.text = "{\"status\": 404,  \"error_message\": \"The backend with id 123 could not be found\"}"
        mock_response.raise_for_status.side_effect = HTTPError("Error", response=mock_response)

        mock_get.return_value = mock_response
        mock_get.__name__ = "get"

        # When
        with self.assertRaises(PlanqkClientError) as error_response:
            self._client.get_backend("123")

        # Then
        self.assertEqual(str(error_response.exception),
                         "The backend with id 123 could not be found (HTTP error: 404)")

    def assert_backend(self, expected: dict, actual: BackendDto):
        # main attributes
        self.assertEqual(expected["id"], actual.id)
        self.assertEqual(expected["internal_id"], actual.internal_id)
        self.assertEqual(expected["provider"], actual.provider.value)
        self.assertEqual(expected["hardware_provider"], actual.hardware_provider.value)
        self.assertEqual(expected["name"], actual.name)
        self.assertEqual(expected["type"], actual.type.value)
        self.assertEqual(expected["status"], actual.status.value)
        self.assertEqual(expected["avg_queue_time"], actual.avg_queue_time)

        # documentation
        doc = expected["documentation"]
        self.assertEqual(doc["description"], actual.documentation.description)
        self.assertEqual(doc["url"], actual.documentation.url)
        self.assertEqual(doc["location"], actual.documentation.location)

        # configuration
        config = expected["configuration"]
        self.assertEqual(len(config["gates"]), len(actual.configuration.gates))
        self.assertEqual(len(config["qubits"]), len(actual.configuration.qubits))
        self.assertEqual(config["qubit_count"], actual.configuration.qubit_count)
        self.assertEqual(config["connectivity"]["fully_connected"], actual.configuration.connectivity.fully_connected)
        self.assertEqual(config["supported_input_formats"][0], actual.configuration.supported_input_formats[0].value)
        self.assertEqual(config["shots_range"]["min"], actual.configuration.shots_range.min)
        self.assertEqual(config["shots_range"]["max"], actual.configuration.shots_range.max)
        self.assertEqual(config["memory_result_supported"], actual.configuration.memory_result_supported)

        # gates
        for gate_mock, gate in zip(config["gates"], actual.configuration.gates):
            self.assertEqual(gate_mock["name"], gate.name)
            self.assertEqual(gate_mock["native_gate"], gate.native_gate)

        # qubits
        for qubit_mock, qubit in zip(config["qubits"], actual.configuration.qubits):
            self.assertEqual(qubit_mock["id"], qubit.id)

        # availability
        for times_mock, times in zip(expected["availability"], actual.availability):
            self.assertEqual(times_mock["granularity"], times.granularity)
            self.assertEqual(times_mock["start"], str(times.start))
            self.assertEqual(times_mock["end"], str(times.end))

        # costs
        for cost_mock, cost in zip(expected["costs"], actual.costs):
            self.assertEqual(cost_mock["granularity"], cost.granularity)
            self.assertEqual(cost_mock["currency"], cost.currency)
            self.assertEqual(cost_mock["value"], cost.value)
