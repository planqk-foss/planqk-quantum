import unittest
from typing import Dict, List

import rustworkx as rx

from planqk.client.backend_dtos import ConfigurationDto, QubitDto, ConnectivityDto, ShotsRangeDto
from planqk.qiskit import PlanqkQiskitBackend


def get_configuration_with_coupling_map(qubits: List[QubitDto], connectivity_graph: Dict[str, List[str]]):
    configuration = ConfigurationDto(
        gates=[],
        instructions=[],
        qubit_count=len(qubits),
        qubits=qubits,
        connectivity=ConnectivityDto(
            fully_connected=False,
            graph=connectivity_graph
        ),
        supported_input_formats=["QISKIT"],
        shots_range=ShotsRangeDto(min=1, max=1000)
    )
    return configuration


class TestPlanqkQiskitTarget(unittest.TestCase):
    def test_building_coupling_map_with_coupling_graph_starting_at_zero(self):
        # Given
        qubits = [QubitDto(id=str(i)) for i in range(4)]
        connectivity_graph = {"0": ["1"], "1": ["2"], "2": ["3"]}
        configuration = get_configuration_with_coupling_map(qubits, connectivity_graph)

        target = PlanqkQiskitBackend.PlanqkQiskitTarget(configuration=configuration)
        target._gate_map = {'cp': {(0, 1): None, (1, 2): None, (2, 3): None},
                             'cx': {(0, 1): None, (1, 2): None, (2, 3): None},
                             'x': {(0,): None, (1,): None, (2,): None, (3,): None}}


        # When
        coupling_map = target.build_coupling_map()

        # Then
        expected_graph = rx.PyDiGraph()
        expected_graph.add_nodes_from([None for _ in range(4)])
        expected_graph.add_edges_from([(0, 1, None), (1, 2, None), (2, 3, None)])

        actual_graph = coupling_map.graph
        self.assertEqual(expected_graph.nodes(), actual_graph.nodes())
        self.assertCountEqual(expected_graph.nodes(), actual_graph.nodes())


