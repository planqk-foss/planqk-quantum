import unittest
from unittest.mock import patch, MagicMock

from requests import Response

from planqk import PlanqkQuantumProvider
from planqk.exceptions import PlanqkClientError, BackendNotFoundError, PlanqkError
from planqk.qiskit import PlanqkQuantumProvider as LegacyProvider1
from planqk.qiskit.provider import PlanqkQuantumProvider as LegacyProvider2
from planqk.qiskit.providers.aws.aws_backend import PlanqkAwsBackend
from planqk.qiskit.providers.azure.ionq_backend import PlanqkAzureIonqBackend
from tests.testobjects.test_objects import TestObjects


class ProviderTestSuite(unittest.TestCase):

    def test_instantiate_provider_class(self):
        p = PlanqkQuantumProvider()
        self.assertIsNotNone(p)

    def test_instantiate_legacy_provider_1_class(self):
        p = LegacyProvider1()
        self.assertIsNotNone(p)

    def test_instantiate_legacy_provider_2_class(self):
        p = LegacyProvider2()
        self.assertIsNotNone(p)

    def test_backend_id_class_mappings(self):
        provider = PlanqkQuantumProvider()
        self.assertEqual(9, len(provider._backend_mapping))

    @patch("planqk.client.client._PlanqkClient")
    def test_get_backends(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        sv1 = TestObjects.backends().dtos().aws_sv1_backend()
        aquila = TestObjects.backends().dtos().aws_quera_backend()
        client_mock.get_backends.return_value = [aquila, sv1]

        # When
        backend_ids = PlanqkQuantumProvider(_client=client_mock).backends()

        # Then
        self.assertEqual(2, len(backend_ids))
        self.assertEqual(aquila.id, backend_ids[0])
        self.assertEqual(sv1.id, backend_ids[1])


    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_raise_backend_not_found_error(self, get_backend_mock):
        response = Response()
        response.status_code = 404
        get_backend_mock.side_effect = PlanqkClientError(response)
        provider = PlanqkQuantumProvider()
        with self.assertRaises(BackendNotFoundError):
            provider.get_backend(backend_id="ibmq_qasm_simulator")

    @patch("planqk.client.client._PlanqkClient.get_backend_state")
    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_get_aws_sv1_qiskit_backend(self, get_backend_mock, get_backend_state_mock):
        get_backend_mock.return_value = TestObjects.backends().dtos().aws_sv1_backend()
        get_backend_state_mock.return_value = TestObjects.backends().dtos().state_online()

        backend = PlanqkQuantumProvider().get_backend("aws.sim.sv1")
        self.assertIsNotNone(backend)
        self.assertIsInstance(backend, PlanqkAwsBackend)

    @patch("planqk.client.client._PlanqkClient.get_backend_state")
    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_get_aws_iqm_garnet_qiskit_backend(self, get_backend_mock, get_backend_state_mock):
        get_backend_mock.return_value = TestObjects.backends().dtos().aws_iqm_garnet_backend()
        get_backend_state_mock.return_value = TestObjects.backends().dtos().state_online()

        backend = PlanqkQuantumProvider().get_backend("aws.iqm.garnet")
        self.assertIsNotNone(backend)
        self.assertIsInstance(backend, PlanqkAwsBackend)

    @patch("planqk.client.client._PlanqkClient.get_backend_state")
    @patch("planqk.client.client._PlanqkClient.get_backend")
    def test_get_azure_ionq_sim_qiskit_backend(self, get_backend_mock, get_backend_state_mock):
        get_backend_mock.return_value = TestObjects.backends().dtos().azure_ionq_sim_backend()
        get_backend_state_mock.return_value = TestObjects.backends().dtos().state_online()

        backend = PlanqkQuantumProvider().get_backend("azure.ionq.simulator")
        self.assertIsNotNone(backend)
        self.assertIsInstance(backend, PlanqkAzureIonqBackend)

    @patch("planqk.client.client._PlanqkClient")
    def test_get_jobs(self, client_mock_class: MagicMock):
        # Given : Qiskit and AHS jobs
        client_mock = client_mock_class.return_value
        qiskit_job_dto = TestObjects.jobs().dtos().aws_sv1_job()
        braket_task_dto = TestObjects.jobs().dtos().aws_quera_task()
        client_mock.get_jobs.return_value = [qiskit_job_dto, braket_task_dto]

        # When do not fetch state from client Queued state should be returned
        jobs = PlanqkQuantumProvider(_client=client_mock).jobs()
        self.assertEqual(2, len(jobs))
        self.assertEqual(qiskit_job_dto.id, jobs[0].id)
        self.assertEqual(qiskit_job_dto.provider, jobs[0].provider)
        self.assertEqual(qiskit_job_dto.backend_id, jobs[0].backend_id)
        self.assertEqual(qiskit_job_dto.created_at, jobs[0].created_at)

        self.assertEqual(braket_task_dto.id, jobs[1].id)
        self.assertEqual(braket_task_dto.provider, jobs[1].provider)
        self.assertEqual(braket_task_dto.backend_id, jobs[1].backend_id)
        self.assertEqual(braket_task_dto.created_at, jobs[1].created_at)

    @patch("planqk.client.client._PlanqkClient")
    def test_retrieve_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        qiskit_job_dto = TestObjects.jobs().dtos().aws_sv1_job()
        client_mock.get_job.return_value = qiskit_job_dto

        # When
        job = PlanqkQuantumProvider(_client=client_mock).retrieve_job(qiskit_job_dto.id)
        self.assertEqual(qiskit_job_dto.id, job.id)
        self.assertEqual(qiskit_job_dto.shots, job.shots)

    @patch("planqk.client.client._PlanqkClient")
    def test_should_raise_client_error_when_job_not_found(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        response = Response()
        response.status_code = 404
        client_mock.get_job.side_effect = PlanqkClientError(response)

        # When / Then
        with self.assertRaises(PlanqkClientError):
            PlanqkQuantumProvider(_client=client_mock).retrieve_job("job_id")

    @patch("planqk.client.client._PlanqkClient")
    def test_should_raise_planqk_error_when_job_is_not_supported_by_qiskit(self, client_mock_class: MagicMock):
        # Given QuEra task which cannot be processed by Qiskit
        client_mock = client_mock_class.return_value
        job_dto = TestObjects.jobs().dtos().aws_quera_task()
        client_mock.get_job.return_value = job_dto

        # When / Then
        with self.assertRaises(PlanqkError):
            PlanqkQuantumProvider(_client=client_mock).retrieve_job(job_dto.id)


