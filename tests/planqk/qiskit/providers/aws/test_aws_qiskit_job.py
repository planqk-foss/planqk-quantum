import unittest
from unittest.mock import patch, MagicMock

from planqk.client.model_enums import Job_Status
from qiskit.providers import JobStatus
from qiskit.result.models import ExperimentResult
from tests.testobjects.test_objects import TestObjects


class AwsQiskitJobTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_get_job_status(self, client_mock_class: MagicMock):
        client_mock = client_mock_class.return_value
        task = TestObjects.jobs().aws_quera_task(client_mock)
        client_mock.get_job_status.return_value = Job_Status.PENDING

        # When / Then
        self.assertEqual("QUEUED", task.state())

        client_mock.get_job_status.return_value = Job_Status.COMPLETED
        self.assertEqual("COMPLETED", task.state())

    @patch("planqk.client.client._PlanqkClient")
    def test_retrieve_result_of_completed_sv1_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        job = TestObjects.jobs().aws_sv1_job(client_mock)
        client_mock.get_job_result.return_value = TestObjects.jobs().results().aws_sv1_result()
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        result = job.result()

        # Then
        self.assertIsNotNone(result)
        self.assertEqual(True, result.success)
        result_entry: ExperimentResult = result.results[0]
        self.assertEqual(3, result_entry.shots)
        data = result_entry.data
        self.assertIsNotNone(data)
        self.assertEqual(3, data.counts['111'])
        self.assertEqual(['111', '111', '111'], data.memory)
        self.assertEqual("circ0", result_entry.header.name)
        self.assertEqual(JobStatus.DONE.name, result_entry.status)

    @patch("planqk.client.client._PlanqkClient")
    def test_retrieve_result_of_completed_ionq_aria_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        job = TestObjects.jobs().aws_ionq_aria_job(client_mock)
        client_mock.get_job_result.return_value = TestObjects.jobs().results().aws_ionq_aria_result()
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        result = job.result()

        # Then
        self.assertIsNotNone(result)
        self.assertEqual(True, result.success)
        result_entry: ExperimentResult = result.results[0]
        self.assertEqual(3, result_entry.shots)
        data = result_entry.data
        self.assertIsNotNone(data)
        self.assertEqual(2, data.counts['000'])
        self.assertEqual(1, data.counts['111'])
        self.assertEqual(['000', '000', '111'], data.memory)
        self.assertEqual("circ0", result_entry.header.name)
        self.assertEqual(JobStatus.DONE.name, result_entry.status)

    @patch("planqk.client.client._PlanqkClient")
    def test_raise_error_when_retrieving_result_failed_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        job = TestObjects.jobs().aws_sv1_job(client_mock)
        error_result = TestObjects.jobs().results().error_result()
        client_mock.get_job_result.return_value = error_result
        client_mock.get_job_status.return_value = Job_Status.FAILED

        # When

        with self.assertRaises(RuntimeError) as context:
            job.result()

        # Optionally check the error message
        self.assertIn(f"Cannot retrieve results because the job execution failed with status 'FAILED'. Reason: {str(error_result)}", str(context.exception))

    @patch("planqk.client.client._PlanqkClient")
    def test_cancel_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        job = TestObjects.jobs().aws_sv1_job(client_mock)

        # When
        job.cancel()

        # Then
        client_mock.cancel_job.assert_called_once_with(job.id)
