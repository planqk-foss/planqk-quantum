import unittest
from unittest.mock import patch, MagicMock

from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Job_Status, Job_Input_Format, PlanqkSdkProvider
from planqk.qiskit.providers.aws.aws_qiskit_job import PlanqkAwsQiskitJob
from tests.testobjects.test_objects import TestObjects


class AwsQiskitBackendTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_run_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        backend = TestObjects.backends().aws_ionq_aria_backend(client_mock)
        circuit = TestObjects.jobs().inputs().qiskit_3_ghz()

        # When
        backend.run(job_input=circuit)

        # Then
        exp_job = JobDto(
            provider='AWS',
            shots=1,
            backend_id='aws.ionq.aria',
            id=None,
            provider_job_id=None,
            session_id=None,
            input='OPENQASM 3.0;\n'
                  'bit[3] b;\n'
                  'qubit[3] q;\n'
                  'h q[0];\n'
                  'cnot q[0], q[1];\n'
                  'cnot q[1], q[2];\n'
                  'b[0] = measure q[0];\n'
                  'b[1] = measure q[1];\n'
                  'b[2] = measure q[2];',
            input_format=Job_Input_Format.BRAKET_OPEN_QASM_V3,
            input_params={'disable_qubit_rewiring': False},
            started_at=None,
            created_at=None,
            ended_at=None,
            name=None,
            tags=None,
            sdk_provider=PlanqkSdkProvider.QISKIT
        )
        client_mock.submit_job.assert_called_once_with(exp_job)

    @patch("planqk.client.client._PlanqkClient")
    def test_retrieve_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        exp_job_dto = TestObjects.jobs().dtos().aws_sv1_job()
        client_mock.get_job.return_value = exp_job_dto
        client_mock.get_job_status.return_value = Job_Status.COMPLETED
        backend = TestObjects.backends().aws_sv1_backend(client_mock)

        # When
        job = backend.retrieve_job(job_id=exp_job_dto.id)

        # Then
        self.assertEqual(job.id, exp_job_dto.id)
        self.assertEqual(job.backend(), backend)
        self.assertIsInstance(job, PlanqkAwsQiskitJob)
        self.assertEqual(job._job_details, exp_job_dto)




