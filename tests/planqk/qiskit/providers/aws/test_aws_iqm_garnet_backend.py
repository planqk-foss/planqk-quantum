import unittest
from unittest.mock import patch, MagicMock

import rustworkx as rx

from tests.testobjects.test_objects import TestObjects


def build_expected_graph():
    # Add nodes starting at 0 with range
    expected = rx.PyDiGraph()
    expected.add_nodes_from([None for _ in range(20)])

    edge_value = {'cp': None, 'cx': None, 'cy': None, 'cz': None, 'ecr': None, 'iswap': None, 'rxx': None, 'ryy': None, 'rzz': None, 'swap': None}
    expected.add_edges_from([
        (9, 10, edge_value), (9, 14, edge_value), (9, 4, edge_value), (9, 8, edge_value), (10, 11, edge_value), (10, 15, edge_value), (10, 5, edge_value), (11, 16, edge_value),
        (11, 6, edge_value), (12, 13, edge_value), (12, 7, edge_value), (13, 14, edge_value), (13, 17, edge_value), (13, 8, edge_value), (14, 15, edge_value), (14, 18, edge_value),
        (15, 16, edge_value), (15, 19, edge_value), (17, 18, edge_value), (18, 19, edge_value), (0, 1, edge_value), (0, 3, edge_value), (1, 4, edge_value), (2, 3, edge_value), (2, 7, edge_value),
        (3, 4, edge_value), (3, 8, edge_value), (4, 5, edge_value), (5, 6, edge_value), (7, 8, edge_value)
    ])

    return  expected


class AwsIqmGarnetBackendTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_coupling_map(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        backend = TestObjects.backends().aws_iqm_garnet_backend(client_mock)

        # When
        coupling_map = backend.target.build_coupling_map()

        exp_graph = build_expected_graph()
        actual_graph = coupling_map.graph

        self.assertCountEqual(exp_graph.nodes(), actual_graph.nodes())
        self.assertCountEqual(exp_graph.edges(), actual_graph.edges())


    @patch("planqk.client.client._PlanqkClient")
    def test_run_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        backend = TestObjects.backends().aws_iqm_garnet_backend(client_mock)
        circuit = TestObjects.jobs().inputs().qiskit_3_ghz()

        # When
        backend.run(job_input=circuit)


