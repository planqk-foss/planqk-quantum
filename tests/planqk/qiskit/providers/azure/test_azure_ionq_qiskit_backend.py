import unittest
from unittest.mock import patch, MagicMock

from planqk.client.job_dtos import JobDto
from planqk.client.model_enums import Job_Input_Format, PlanqkSdkProvider
from tests.testobjects.test_objects import TestObjects


class AzureIonqSimQiskitBackendTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_run_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        backend = TestObjects.backends().azure_ionq_sim_backend(client_mock)
        circuit = TestObjects.jobs().inputs().qiskit_3_ghz()

        # When
        backend.run(job_input=circuit, memory=True)

        # Then
        exp_job = JobDto(
            provider='AZURE',
            shots=1,
            backend_id='azure.ionq.simulator',
            id=None,
            provider_job_id=None,
            session_id=None,
            input={'gateset': 'qis',
                   'qubits': 3,
                   'circuit': [{'gate': 'h', 'targets': [0]},
                               {'gate': 'x', 'targets': [1], 'controls': [0]},
                               {'gate': 'x', 'targets': [2], 'controls': [1]}]},
            input_format=Job_Input_Format.IONQ_CIRCUIT_V1,
            input_params={'memory': True},
            started_at=None,
            created_at=None,
            ended_at=None,
            name=None,
            tags=None,
            sdk_provider=PlanqkSdkProvider.QISKIT
        )
        client_mock.submit_job.assert_called_once_with(exp_job)
