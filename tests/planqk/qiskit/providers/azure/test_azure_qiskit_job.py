import unittest
from unittest.mock import patch, MagicMock

from planqk.client.model_enums import Job_Status
from qiskit.providers import JobStatus
from qiskit.result.models import ExperimentResult
from tests.testobjects.test_objects import TestObjects


class AzureQiskitJobTestSuite(unittest.TestCase):

    @patch("planqk.client.client._PlanqkClient")
    def test_retrieve_result_of_completed_ionq_sim_job(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        job = TestObjects.jobs().azure_ionq_sim_job(client_mock)
        client_mock.get_job_result.return_value = TestObjects.jobs().results().azure_ionq_sim_result()
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        result = job.result()

        # Then
        self.assertIsNotNone(result)
        self.assertEqual(True, result.success)
        result_entry: ExperimentResult = result.results[0]
        self.assertEqual(3, result_entry.shots)
        data = result_entry.data
        self.assertIsNotNone(data)
        self.assertEqual(3, data.counts['100'])
        self.assertEqual(['100', '100', '100'], data.memory)
        self.assertEqual("circ0", result_entry.header.name)
        self.assertEqual(JobStatus.DONE.name, result_entry.status)

    @patch("planqk.client.client._PlanqkClient")
    def test_retrieve_result_of_completed_ionq_sim_job_memory(self, client_mock_class: MagicMock):
        # Given
        client_mock = client_mock_class.return_value
        job = TestObjects.jobs().azure_ionq_sim_job(client_mock)
        client_mock.get_job_result.return_value = TestObjects.jobs().results().azure_ionq_sim_result()
        client_mock.get_job_status.return_value = Job_Status.COMPLETED

        # When
        result = job.result()

        # Then
        self.assertIsNotNone(result)
        self.assertEqual(True, result.success)
        result_entry: ExperimentResult = result.results[0]
        self.assertEqual(3, result_entry.shots)
        data = result_entry.data
        self.assertIsNotNone(data)
        self.assertEqual(3, data.counts['100'])
        self.assertEqual("circ0", result_entry.header.name)
        self.assertEqual(JobStatus.DONE.name, result_entry.status)

