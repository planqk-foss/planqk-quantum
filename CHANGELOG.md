# Release Notes for PLANQK Quantum SDK

## [2.15.0] - 2025-02-09

### Added

- Jobs now indicate whether they were created using Qiskit or Braket.

## [2.14.2] - 2025-01-17

### Changed

- Ensure that Qiskit jobs can be only retrieved from backends supported by Qiskit
- Ensure that Braket tasks can be only retrieved from AWS backends

## [2.14.1] - 2024-12-17

### Added

- Import `planqk.braket.PlanqkAwsQuantumTask` representing Braket tasks

### Changed

- Renamed function `list_supported_devices` in `PlanqkBraketProvider` to `devices`.

## [2.14.0] - 2024-12-13

### Added

- Introduce `PlanqkBraketProvider` for accessing PLANQK backends via Braket SDK

The
`PlanqkBraketProvider` integrates PLANQK backends with the AWS Braket SDK, allowing developers to interact seamlessly with Braket device objects and create quantum tasks.
To access for instance IonQ Aria you can perform:

```python
device = PlanqkBraketProvider.get_device("aws.ionq.aria")
circuit = ...
device.run(circuit, shots=100)
```

### Changed

- Updated `PlanqkBraketProvider` to provide access exclusively to Qiskit backends.
