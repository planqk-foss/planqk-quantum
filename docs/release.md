# Release Process

The SDK is released to [PyPi](https://pypi.org/project/planqk-quantum).
The release numbers follow the [Semantic Versioning](https://semver.org) approach resulting in a version number in the format `major.minor.patch`.
The version number is automatically increased by the CI/CD pipeline based on your [commit message](https://github.com/semantic-release/semantic-release#commit-message-format).

## Production Release

If you push to the `main` branch and the commit message contains, for example, the prefix `feat:`, `fix:` or `perf:` a new release will be created automatically.
You can use the keyword `BREAKING CHANGE` in the commit message to trigger a major release.
Use `chore:` or omit the prefix if you do not want a new release to be created.

**Warning:** This release will be public and affects all services using the SDK in production.

## Staging / Testing Release

If you want to create a release only for the testing environment (pre-release), perform the following steps:

1. Create a new branch from `main` and name it `dev` (make sure you delete an old `dev` branch).
   This branch is used for pre-releases and its commits are not automatically released.
2. In the `dev` branch open `setup.py` file and increase the version number and add the suffix `-rcX` to it, where
   `X` is the release candidate number.
   If the version number is for instance `12.1.0`, then the new version number should be `12.2.0-rc1`.
3. Push your changes to the `dev` branch.
4. Go to the [GitLab repository](https://gitlab.com/planqk-foss/planqk-quantum) and click in section `Deploy` on `Releases`.
5. Click on `Draft a new release`.
6. Click on `Choose a tag`.
   Enter the new version number prefixed by `v`in the `Tag version` field, e.g., `v12.2.0-rc1`.
7. Select the `dev` branch in the `Target` field.
8. Enter as title the tag name and add a description for the release.
9. Click on `Publish release`.

Do not forget to merge your changes back to the main branch once you want to release these changes to production.
